package com.app.news.di.modules;


import dagger.Module;


/**
 * All fragments must be specified here
 */
@Module
abstract class FragmentBuildersModule {


}

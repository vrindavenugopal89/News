package com.app.news.core;


import com.app.news.model.NFeedResponse;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface Api {

    @GET("v2/everything")
    Observable<NFeedResponse> getAllArticles(
            @QueryMap Map<String, String> articles);

    @GET("v2/top-headlines")
    Observable<NFeedResponse> getTopRatingArticles(
            @QueryMap Map<String, String> articles);


}

package com.app.news.di.modules;

import android.app.Application;
import android.content.Context;

import com.app.news.core.Api;
import com.app.news.core.DataManager;
import com.app.news.core.schedulers.BaseSchedulers;
import com.app.news.core.schedulers.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {ViewModelModule.class})
public class AppModule {
    /**
     * Provides schedulers for thread related operations
     *
     * @return
     */
    @Provides
    @Singleton
    BaseSchedulers provideScheduler() {
        return new SchedulerProvider();
    }

    /**
     * Provides API
     *
     * @param retrofit
     * @return
     */
    @Provides
    @Singleton
    Api provideApi(Retrofit retrofit) {
        return retrofit.create(Api.class);
    }

    /**
     * Provides DataManager to perform API calling
     *
     * @param api
     * @param scheduler
     * @return
     */


    @Provides
    @Singleton
    DataManager provideDataManager(Api api, BaseSchedulers scheduler) {
        return new DataManager(api, scheduler);
    }

    /**
     * provides context
     * @param application
     * @return
     */

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }


}

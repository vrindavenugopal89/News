package com.app.news.view.feed;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import com.app.news.BR;
import com.app.news.R;
import com.app.news.core.NfeedConstants;
import com.app.news.view.BaseActivity;
import com.app.news.databinding.ActivityNewsFeedBinding;
import com.app.news.model.NFeedResponse;
import com.app.news.vm.NFeedVM;

import javax.inject.Inject;

public class NFeedActivity extends BaseActivity<ActivityNewsFeedBinding, NFeedVM> implements NFeedNavigator {

    @Inject
    NFeedVM nFeedVM;


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_news_feed;
    }

    @Override
    public NFeedVM getViewModel() {
        return nFeedVM;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityNewsFeedBinding binding = getViewDataBinding();
        nFeedVM.setNavigator(this);
        if (getIntent().getExtras() != null) {
            NFeedResponse response = getIntent().getParcelableExtra(NfeedConstants.NFEED);
            String title = getIntent().getStringExtra(NfeedConstants.NFEED_TITLE);
            binding.titleTV.setText(title);
            if (response != null) {
                binding.newsRV.setLayoutManager(new LinearLayoutManager(this));
                NFeedAdapter adapter = new NFeedAdapter(response);
                adapter.setListener(articles -> {
                    if (articles != null) {
                        Intent intent = new Intent(NFeedActivity.this, NFeedDetailsActivity.class);
                        intent.putExtra(NfeedConstants.NFEED, articles);
                        startActivity(intent);
                    }
                });
                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(NFeedActivity.this,
                        DividerItemDecoration.VERTICAL);
                binding.newsRV.addItemDecoration(dividerItemDecoration);
                binding.newsRV.setNestedScrollingEnabled(false);
                binding.newsRV.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onClosePage() {
        finish();
    }
}

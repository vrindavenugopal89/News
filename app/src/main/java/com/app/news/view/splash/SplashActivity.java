package com.app.news.view.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.app.news.BR;
import com.app.news.R;
import com.app.news.databinding.ActivityMainBinding;
import com.app.news.view.BaseActivity;
import com.app.news.view.home.HomeActivity;
import com.app.news.vm.SplashVM;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity<ActivityMainBinding, SplashVM> {
    @Inject
    SplashVM splashVM;
    private Handler mWaitHandler = new Handler();

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public SplashVM getViewModel() {
        return splashVM;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goHomeAfterDelay();
    }

    private void goHomeAfterDelay() {
        mWaitHandler.postDelayed(() -> {
            try {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                SplashActivity.this.startActivity(intent);
                SplashActivity.this.finish();
            } catch (Exception ignored) {
                ignored.printStackTrace();
            }
        }, 5000);
    }


}

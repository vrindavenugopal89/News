package com.app.news.view.feed;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.app.news.BR;
import com.app.news.R;
import com.app.news.core.NfeedConstants;
import com.app.news.view.BaseActivity;
import com.app.news.databinding.ActivityNfeedDetailsHomeBinding;
import com.app.news.model.NFeedResponse;
import com.app.news.vm.NFeedDetailsVM;

import javax.inject.Inject;

public class NFeedDetailsActivity extends BaseActivity<ActivityNfeedDetailsHomeBinding, NFeedDetailsVM> implements NFeeddetailsNavigator {
    @Inject
    NFeedDetailsVM nFeedDetailsVM;


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_nfeed_details_home;
    }

    @Override
    public NFeedDetailsVM getViewModel() {
        return nFeedDetailsVM;
    }

    @Override
    public void onClosePage() {
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityNfeedDetailsHomeBinding binding = getViewDataBinding();
        nFeedDetailsVM.setNavigator(this);
        if (getIntent().getExtras() != null) {
            NFeedResponse.Articles articles = getIntent().getParcelableExtra(NfeedConstants.NFEED);
            if (articles != null) {
                binding.setData(articles);
            }
        }
    }
}

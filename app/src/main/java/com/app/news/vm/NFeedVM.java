package com.app.news.vm;

import com.app.news.core.DataManager;
import com.app.news.view.feed.NFeedNavigator;

import javax.inject.Inject;

public class NFeedVM extends BaseViewModel<NFeedNavigator> {

    @Inject
    public NFeedVM(DataManager dataManager) {
        super(dataManager);
    }

    public void onClosePage(){
        getNavigator().onClosePage();
    }

}

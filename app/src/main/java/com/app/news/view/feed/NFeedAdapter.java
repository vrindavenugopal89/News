package com.app.news.view.feed;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.news.R;
import com.app.news.model.NFeedResponse;
import com.app.news.databinding.AdapterNfeedItemViewBinding;

public class NFeedAdapter extends RecyclerView.Adapter<NFeedAdapter.NFeedVH> {
    private NFeedResponse response;
    private OnNFeedListener listener;

    public NFeedAdapter(NFeedResponse response) {
        this.response = response;
    }

    @NonNull
    @Override
    public NFeedVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        View statusContainer = LayoutInflater.from(context).inflate(R.layout.adapter_nfeed_item_view, viewGroup, false);
        return new NFeedVH(statusContainer);
    }

    @Override
    public void onBindViewHolder(@NonNull NFeedVH nFeedVH, int i) {
        nFeedVH.bindData(response.getArticles().get(i));
        nFeedVH.itemView.setOnClickListener(v -> listener.onFeedSelected(response.getArticles().get(i)));
    }

    @Override
    public int getItemCount() {
        return response.getArticles() != null ? response.getArticles().size() : 0;
    }

    public void setListener(OnNFeedListener listener) {
        this.listener = listener;
    }

    public interface OnNFeedListener {
        void onFeedSelected(NFeedResponse.Articles articles);
    }

    public class NFeedVH extends RecyclerView.ViewHolder {
        AdapterNfeedItemViewBinding binding;

        public NFeedVH(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public void bindData(NFeedResponse.Articles articles) {
            binding.setData(articles);
        }
    }
}

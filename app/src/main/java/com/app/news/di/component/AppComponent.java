
package com.app.news.di.component;

import android.app.Application;

import com.app.news.core.MvvmApp;
import com.app.news.di.modules.ActivityModule;
import com.app.news.di.modules.AppModule;
import com.app.news.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;


@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        NetworkModule.class,
        ActivityModule.class}
)
public interface AppComponent {

    void inject(MvvmApp app);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}

package com.app.news.view.home;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.news.R;
import com.app.news.model.NFeedMenu;
import com.app.news.databinding.AdapterMenuItemViewBinding;

import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeVH> {

    private List<NFeedMenu> menus;
    private OnMenuListener onMenuListener;

    public HomeAdapter(List<NFeedMenu> menuList) {
        this.menus = menuList;
    }

    @NonNull
    @Override
    public HomeVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        View statusContainer = LayoutInflater.from(context).inflate(R.layout.adapter_menu_item_view, viewGroup, false);
        return new HomeVH(statusContainer);

    }

    @Override
    public void onBindViewHolder(@NonNull HomeVH homeVH, int i) {
        homeVH.binData(menus.get(i));
        homeVH.itemView.setOnClickListener(v -> {
            if (onMenuListener != null) {
                onMenuListener.onOptionSelected(menus.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return menus != null ? menus.size() : 0;
    }

    public void setOnMenuListener(OnMenuListener onMenuListener) {
        this.onMenuListener = onMenuListener;
    }

    public interface OnMenuListener {
        void onOptionSelected(NFeedMenu nFeedMenu);
    }

    public class HomeVH extends RecyclerView.ViewHolder {
        private AdapterMenuItemViewBinding adapterMenuItemViewBinding;

        public HomeVH(@NonNull View itemView) {
            super(itemView);
            adapterMenuItemViewBinding = DataBindingUtil.bind(itemView);

        }

        public void binData(NFeedMenu menu) {
            adapterMenuItemViewBinding.setData(menu);

        }
    }
}

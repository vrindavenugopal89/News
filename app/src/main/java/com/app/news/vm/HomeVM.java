package com.app.news.vm;

import android.arch.lifecycle.MutableLiveData;

import com.app.news.core.DataManager;
import com.app.news.core.NfeedConstants;
import com.app.news.model.NFeedError429;
import com.app.news.model.NFeedMenu;
import com.app.news.model.NFeedResponse;
import com.app.news.model.response.ApiResponse;
import com.app.news.model.response.ResponseListener;
import com.app.news.view.home.HomeNavigator;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class HomeVM extends BaseViewModel<HomeNavigator> {
    public MutableLiveData<ApiResponse<NFeedMenu>> nfeedresponse = new MutableLiveData<>();
    private DataManager mdataManager;


    @Inject
    public HomeVM(DataManager dataManager) {
        super(dataManager);
        mdataManager = dataManager;

    }

    public void onClosePage() {
        getNavigator().onClosePage();
    }


    public void fetchNFeedMenu(int id) {
        Map<String, String> data;
        switch (id) {
            case 1:
                data = new HashMap<>();
                data.put(NfeedConstants.NFEED_Q, NfeedConstants.NFEED_BITCOIN);
                data.put(NfeedConstants.NFEED_FROM, NfeedConstants.NFEED_FROM_DATE);
                data.put(NfeedConstants.NFEED_SORTBY, NfeedConstants.NFEED_PUBLISHED_AT);
                data.put(NfeedConstants.NFEED_APIKEY, NfeedConstants.API_KEY);
                getBitCoinArticle(data);
                break;
            case 2:
                data = new HashMap<>();
                data.put(NfeedConstants.NFEED_COUNTRY, NfeedConstants.NFEED_COUNTRY_NAME);
                data.put(NfeedConstants.NFEED_CATEGORY, NfeedConstants.NFEED_CATEGORY_NAME);
                data.put(NfeedConstants.NFEED_APIKEY, NfeedConstants.API_KEY);
                getBusinessArticle(data);
                break;
            case 3:
                data = new HashMap<>();
                data.put(NfeedConstants.NFEED_Q, NfeedConstants.NFEED_APPLE);
                data.put(NfeedConstants.NFEED_FROM, NfeedConstants.NFEED_TO_DATE);
                data.put(NfeedConstants.NFEED_TO, NfeedConstants.NFEED_TO_DATE);
                data.put(NfeedConstants.NFEED_SORTBY, NfeedConstants.NFEED_POPULARITY);
                data.put(NfeedConstants.NFEED_APIKEY, NfeedConstants.API_KEY);
                getAppleArticles(data);
                break;
            case 4:
                data = new HashMap<>();
                data.put(NfeedConstants.NFEED_SOURCES, NfeedConstants.NFEED_SOURCES_TECHCRUNCH);
                data.put(NfeedConstants.NFEED_APIKEY, NfeedConstants.API_KEY);
                getTechCrunchArticles(data);
                break;
            case 5:
                data = new HashMap<>();
                data.put(NfeedConstants.NFEED_DOMAIN, NfeedConstants.NFEED_DOMAIN_WSJ);
                data.put(NfeedConstants.NFEED_APIKEY, NfeedConstants.API_KEY);
                getWallStreetJournal(data);
                break;
        }
    }

    private void getBitCoinArticle(Map<String, String> data) {

        mdataManager.getBitCoinFeeds(data, new ResponseListener<NFeedResponse>() {
            @Override
            public void onStart() {
                getNavigator().showLoading();
            }

            @Override
            public void onFinish() {
                getNavigator().hideLoading();
            }

            @Override
            public void onResponse(ApiResponse<NFeedResponse> apiResponse) {
                parseResponse(apiResponse);
                getNavigator().hideLoading();
            }
        });
    }

    private void getBusinessArticle(Map<String, String> data) {

        mdataManager.getBusinessArticle(data, new ResponseListener<NFeedResponse>() {
            @Override
            public void onStart() {
                getNavigator().showLoading();
            }

            @Override
            public void onFinish() {
                getNavigator().hideLoading();
            }

            @Override
            public void onResponse(ApiResponse<NFeedResponse> apiResponse) {
                parseResponse(apiResponse);
                getNavigator().hideLoading();
            }
        });
    }

    private void parseResponse(ApiResponse<NFeedResponse> apiResponse) {
        switch (apiResponse.errorCode) {
            case NfeedConstants.NFEED_CODE200:
                getNavigator().onFetchBusinessArticleSuccess(apiResponse.data);
                break;
            case NfeedConstants.NFEED_CODE429:
                getNavigator().onFetchDataError429(new Gson().fromJson(apiResponse.errorDescription, NFeedError429.class));
                break;
            case NfeedConstants.NFEED_CODE500:
                getNavigator().onError500(apiResponse.errorDescription);
                break;
            default:
                getNavigator().onError500(apiResponse.errorDescription);
                break;

        }
    }

    private void getAppleArticles(Map<String, String> data) {
        mdataManager.getAppleArticles(data, new ResponseListener<NFeedResponse>() {
            @Override
            public void onStart() {
                getNavigator().showLoading();
            }

            @Override
            public void onFinish() {
                getNavigator().hideLoading();
            }

            @Override
            public void onResponse(ApiResponse<NFeedResponse> apiResponse) {
                parseResponse(apiResponse);
                getNavigator().hideLoading();
            }
        });
    }

    private void getTechCrunchArticles(Map<String, String> data) {

        mdataManager.getTechCrunchArticles(data, new ResponseListener<NFeedResponse>() {
            @Override
            public void onStart() {
                getNavigator().showLoading();
            }

            @Override
            public void onFinish() {
                getNavigator().hideLoading();
            }

            @Override
            public void onResponse(ApiResponse<NFeedResponse> apiResponse) {
                parseResponse(apiResponse);
                getNavigator().hideLoading();
            }
        });

    }

    private void getWallStreetJournal(Map<String, String> data) {
        mdataManager.getWallStreetJournal(data, new ResponseListener<NFeedResponse>() {
            @Override
            public void onStart() {
                getNavigator().showLoading();
            }

            @Override
            public void onFinish() {
                getNavigator().hideLoading();
            }

            @Override
            public void onResponse(ApiResponse<NFeedResponse> apiResponse) {
                parseResponse(apiResponse);
                getNavigator().hideLoading();
            }
        });

    }
}

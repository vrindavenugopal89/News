package com.app.news.core;

public class NfeedConstants {
    public static final String NFEED_CODE200 = "200";
    public static final String NFEED_CODE429 = "429";
    public static final String NFEED_CODE500 = "500";
    public static final String API_KEY = "4a5e4285957941e4b9cc5fce697ef931";
    public static final String NFEED = "feed";
    public static final String NFEED_TITLE = "title";
    public static final String NFEED_Q = "q";
    public static final String NFEED_BITCOIN = "bitcoin";
    public static final String NFEED_FROM = "from";
    public static final String NFEED_FROM_DATE = "2018-12-28";
    public static final String NFEED_TO = "to";
    public static final String NFEED_TO_DATE = "2019-01-27";
    public static final String NFEED_SORTBY = "sortBy";
    public static final String NFEED_APIKEY = "apiKey";
    public static final String NFEED_COUNTRY = "country";
    public static final String NFEED_COUNTRY_NAME = "us";
    public static final String NFEED_CATEGORY = "category";
    public static final String NFEED_CATEGORY_NAME = "business";
    public static final String NFEED_APPLE = "apple";
    public static final String NFEED_POPULARITY = "popularity";
    public static final String NFEED_SOURCES = "sources";
    public static final String NFEED_SOURCES_TECHCRUNCH = "techcrunch";
    public static final String NFEED_DOMAIN = "domains";
    public static final String NFEED_DOMAIN_WSJ = "wsj.com";
    public static final String NFEED_PUBLISHED_AT = "publishedAtm";

}

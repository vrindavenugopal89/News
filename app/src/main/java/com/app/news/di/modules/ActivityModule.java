package com.app.news.di.modules;


import com.app.news.view.feed.NFeedActivity;
import com.app.news.view.feed.NFeedDetailsActivity;
import com.app.news.view.home.HomeActivity;
import com.app.news.view.splash.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * All Activity must be specified here
 */

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector(modules = {FragmentBuildersModule.class})
    abstract SplashActivity contributeMainActivity();

    @ContributesAndroidInjector(modules = {FragmentBuildersModule.class})
    abstract HomeActivity contributeHomeActivity();

    @ContributesAndroidInjector(modules = {FragmentBuildersModule.class})
    abstract NFeedActivity contributeNFeedActivity();

    @ContributesAndroidInjector(modules = {FragmentBuildersModule.class})
    abstract NFeedDetailsActivity contributeNFeedDetailsActivity();


}

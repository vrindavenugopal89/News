package com.app.news.model.response;

public class ResponseStatus {
    public static final int SUCCESS = 0;
    public static final int ERROR = 1;
}

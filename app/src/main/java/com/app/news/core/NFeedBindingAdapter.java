package com.app.news.core;

import android.databinding.BindingAdapter;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;

import com.app.news.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class NFeedBindingAdapter {

    @BindingAdapter("image")
    public static void setImage(ImageView image, String url) {
        try {
            Picasso.get()
                    .load(url)
                    .into(image, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            image.setImageDrawable(image.getContext().getResources().getDrawable(R.drawable.ic_placeholder));
                        }
                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    @BindingAdapter("icon")
    public static void loadImage(AppCompatImageView imageView, int resourceID) {
        imageView.setImageResource(resourceID);
    }

    @BindingAdapter("redirect")
    public static void setRedirection(AppCompatTextView textView, String url) {
        textView.setText(Html.fromHtml(
                "<a href=\"" + url + "\"\">google</a> "));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }


}

package com.app.news.view.home;

import com.app.news.model.NFeedError429;
import com.app.news.model.NFeedResponse;

public interface HomeNavigator {

    void onClosePage();

    void showLoading();

    void hideLoading();

    void onFetchBitCoinArticleSuccess(NFeedResponse data);

    void onFetchBusinessArticleSuccess(NFeedResponse data);

    void onFetchApplerticleSuccess(NFeedResponse data);

    void onFetcTechCrunchArticleSuccess(NFeedResponse data);

    void onFetchWallStreetJournalSuccess(NFeedResponse data);

    void onFetchDataError429(NFeedError429 fromJson);

    void onError500(String msg);
}

package com.app.news.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class NFeedResponse implements Parcelable {
    public static final Creator<NFeedResponse> CREATOR = new Creator<NFeedResponse>() {
        @Override
        public NFeedResponse createFromParcel(Parcel in) {
            return new NFeedResponse(in);
        }

        @Override
        public NFeedResponse[] newArray(int size) {
            return new NFeedResponse[size];
        }
    };
    private String status;
    private int totalResults;
    private List<Articles> articles;

    protected NFeedResponse(Parcel in) {
        status = in.readString();
        totalResults = in.readInt();
        articles = in.createTypedArrayList(Articles.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeInt(totalResults);
        dest.writeTypedList(articles);
    }

    public String getStatus() {
        return status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public List<Articles> getArticles() {
        return articles;
    }

    public static class Source implements Parcelable {
        public static final Creator<Source> CREATOR = new Creator<Source>() {
            @Override
            public Source createFromParcel(Parcel in) {
                return new Source(in);
            }

            @Override
            public Source[] newArray(int size) {
                return new Source[size];
            }
        };
        private String id;
        private String name;

        protected Source(Parcel in) {
            id = in.readString();
            name = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    public static class Articles implements Parcelable {
        public static final Creator<Articles> CREATOR = new Creator<Articles>() {
            @Override
            public Articles createFromParcel(Parcel in) {
                return new Articles(in);
            }

            @Override
            public Articles[] newArray(int size) {
                return new Articles[size];
            }
        };
        private String author;
        private String title;
        private String description;
        private String url;
        private String urlToImage;
        private String publishedAt;
        private String content;
        private Source source;

        protected Articles(Parcel in) {
            author = in.readString();
            title = in.readString();
            description = in.readString();
            url = in.readString();
            urlToImage = in.readString();
            publishedAt = in.readString();
            content = in.readString();
            source = in.readParcelable(Source.class.getClassLoader());
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(author);
            dest.writeString(title);
            dest.writeString(description);
            dest.writeString(url);
            dest.writeString(urlToImage);
            dest.writeString(publishedAt);
            dest.writeString(content);
            dest.writeParcelable(source, flags);
        }

        public String getAuthor() {
            return author;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getUrl() {
            return url;
        }

        public String getUrlToImage() {
            return urlToImage;
        }

        public String getPublishedAt() {
            return publishedAt;
        }

        public String getContent() {
            return content;
        }

        public Source getSource() {
            return source;
        }
    }
}

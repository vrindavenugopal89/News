package com.app.news.model;

public class NFeedMenu {
    private String title;
    private String subTitle;
    private int resourceID;
    private int id;


    public NFeedMenu(String title,String subTitle, int resourceID, int id) {
        this.title = title;
        this.subTitle = subTitle;
        this.resourceID = resourceID;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public int getResourceID() {
        return resourceID;
    }

    public int getId() {
        return id;
    }

    public String getSubTitle() {
        return subTitle;
    }
}

package com.app.news.core;


import com.app.news.core.schedulers.BaseSchedulers;
import com.app.news.model.NFeedResponse;
import com.app.news.model.response.ApiResponse;
import com.app.news.model.response.ResponseListener;
import com.app.news.model.response.ResponseStatus;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Class responsible for calling the api services
 */
public class DataManager {

    private Api api;
    private BaseSchedulers scheduler;


    @Inject
    public DataManager(Api api, BaseSchedulers schedulers) {
        this.api = api;
        this.scheduler = schedulers;
    }

    /**
     * Base Method for the response handling
     * <p>
     * All api call should be done using the below method
     */
    private <T> void performRequest(Observable<T> observable, final ResponseListener<T> responseListener) {
        observable.subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(new Observer<T>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        responseListener.onStart();
                    }

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onNext(T t) {
                        responseListener.onResponse(new ApiResponse(ResponseStatus.SUCCESS, t, null));
                    }

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onError(Throwable e) {
                        responseListener.onResponse(new ApiResponse(ResponseStatus.ERROR, null, e));
                    }

                    @Override
                    public void onComplete() {
                        responseListener.onFinish();
                    }
                });


    }

    public void getBitCoinFeeds(Map<String, String> data, ResponseListener<NFeedResponse> responseListener) {
        performRequest(api.getAllArticles(data), responseListener);
    }


    public void getBusinessArticle(Map<String, String> data, ResponseListener<NFeedResponse> responseListener) {
        performRequest(api.getTopRatingArticles(data), responseListener);
    }

    public void getAppleArticles(Map<String, String> data, ResponseListener<NFeedResponse> responseListener) {
        performRequest(api.getAllArticles(data), responseListener);
    }

    public void getTechCrunchArticles(Map<String, String> data, ResponseListener<NFeedResponse> responseListener) {
        performRequest(api.getTopRatingArticles(data), responseListener);
    }

    public void getWallStreetJournal(Map<String, String> data, ResponseListener<NFeedResponse> responseListener) {
        performRequest(api.getAllArticles(data), responseListener);
    }
}

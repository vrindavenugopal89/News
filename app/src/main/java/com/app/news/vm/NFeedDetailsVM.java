package com.app.news.vm;

import com.app.news.core.DataManager;
import com.app.news.view.feed.NFeeddetailsNavigator;

import javax.inject.Inject;

public class NFeedDetailsVM extends BaseViewModel<NFeeddetailsNavigator> {


    @Inject
    public NFeedDetailsVM(DataManager dataManager) {
        super(dataManager);
    }

    public void onClosePage() {
        getNavigator().onClosePage();
    }
}

package com.app.news.di.modules;

import android.arch.lifecycle.ViewModel;

import com.app.news.di.keys.ViewModelKey;
import com.app.news.vm.HomeVM;
import com.app.news.vm.NFeedDetailsVM;
import com.app.news.vm.NFeedVM;
import com.app.news.vm.SplashVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * All viewmodel class must be specified here
 */
@SuppressWarnings("WeakerAccess")
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashVM.class)
    abstract ViewModel bindLoginViewModel(SplashVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(HomeVM.class)
    abstract ViewModel bindHomeVM(HomeVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(NFeedVM.class)
    abstract ViewModel bindNFeedVM(NFeedVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(NFeedDetailsVM.class)
    abstract ViewModel bindNFeedDetailsVM(NFeedVM vm);




}

package com.app.news.core.schedulers;

import android.support.annotation.NonNull;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SchedulerProvider implements BaseSchedulers {

    /**
     * @return creates new threads
     */
    @NonNull
    @Override
    public Scheduler io() {
        return Schedulers.newThread();
    }

    /**
     * which executes actions on the Android main thread
     * @return
     */
    @NonNull
    @Override
    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }
}

package com.app.news.view.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;

import com.app.news.BR;
import com.app.news.R;
import com.app.news.core.NfeedConstants;
import com.app.news.core.utils.NetworkUtils;
import com.app.news.databinding.ActivityNewsHomeBinding;
import com.app.news.model.NFeedError429;
import com.app.news.model.NFeedMenu;
import com.app.news.model.NFeedResponse;
import com.app.news.view.BaseActivity;
import com.app.news.view.feed.NFeedActivity;
import com.app.news.vm.HomeVM;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class HomeActivity extends BaseActivity<ActivityNewsHomeBinding, HomeVM> implements HomeNavigator {

    @Inject
    HomeVM homeVM;
    private ActivityNewsHomeBinding binding;


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_news_home;
    }

    @Override
    public HomeVM getViewModel() {
        return homeVM;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        homeVM.setNavigator(this);
        bindDataInRV(getMenuList());
    }

    public List<NFeedMenu> getMenuList() {
        List<NFeedMenu> menuList = new ArrayList<>();
        menuList.add(new NFeedMenu(getString(R.string.nfeed_str_bitcoin),
                getString(R.string.nfeed_str_bitcoin_sub), R.drawable.ic_bitcoin, 1));
        menuList.add(new NFeedMenu(getString(R.string.nfeed_str_business),
                getString(R.string.nfeed_str_business_sub), R.drawable.ic_business, 2));
        menuList.add(new NFeedMenu(getString(R.string.nfeed_str_apple),
                getString(R.string.nfeed_str_apple_sub), R.drawable.ic_apple, 3));
        menuList.add(new NFeedMenu(getString(R.string.nfeed_str_tech_crunch),
                getString(R.string.nfeed_str_techcrunch_sub), R.drawable.ic_techcrunch, 4));
        menuList.add(new NFeedMenu(getString(R.string.nfeed_wall_street_journal),
                getString(R.string.nfeed_str_wsj_sub), R.drawable.ic_journal, 5));
        return menuList;
    }

    @Override
    public void onClosePage() {
        finish();
    }

    @Override
    public void onFetchBitCoinArticleSuccess(NFeedResponse data) {
        gotoArticleList(getString(R.string.nfeed_str_bitcoin), data);
    }

    private void gotoArticleList(String title, NFeedResponse data) {
        Intent intent = new Intent(HomeActivity.this, NFeedActivity.class);
        intent.putExtra(NfeedConstants.NFEED, data);
        intent.putExtra(NfeedConstants.NFEED_TITLE, title);
        startActivity(intent);
    }

    @Override
    public void onFetchBusinessArticleSuccess(NFeedResponse data) {
        gotoArticleList(getString(R.string.nfeed_str_business), data);
    }

    @Override
    public void onFetchApplerticleSuccess(NFeedResponse data) {
        gotoArticleList(getString(R.string.nfeed_str_apple), data);
    }

    @Override
    public void onFetcTechCrunchArticleSuccess(NFeedResponse data) {
        gotoArticleList(getString(R.string.nfeed_str_tech_crunch), data);
    }

    @Override
    public void onFetchWallStreetJournalSuccess(NFeedResponse data) {
        gotoArticleList(getString(R.string.nfeed_wall_street_journal), data);
    }

    @Override
    public void onFetchDataError429(NFeedError429 fromJson) {
        if (fromJson != null) {
            showAlertDialog(fromJson.getMessage(), this, getString(R.string.nfeed_str_error_json));
        }
    }

    @Override
    public void onError500(String msg) {
        showAlertDialog(msg, this, getString(R.string.nfeed_str_error_json));
    }


    public void bindDataInRV(List<NFeedMenu> menuList) {
        binding.newsRV.setLayoutManager(new LinearLayoutManager(this));
        HomeAdapter adapter = new HomeAdapter(menuList);
        adapter.setOnMenuListener(nFeedMenu -> {
            if (nFeedMenu != null) {
                if (NetworkUtils.isNetworkConnected(this)) {
                    homeVM.fetchNFeedMenu(nFeedMenu.getId());
                } else {
                    showAlertDialog(getString(R.string.nfeed_str_networkerror), this, "no_internet_connection.json");
                }
            }
        });
        binding.newsRV.setNestedScrollingEnabled(false);
        binding.newsRV.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


}

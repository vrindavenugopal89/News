package com.app.news.model;

public class NFeedError429 {
    private String status;
    private String code;
    private String message;

    public String getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}

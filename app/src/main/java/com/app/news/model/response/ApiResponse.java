package com.app.news.model.response;

import android.util.MalformedJsonException;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.HttpException;
import retrofit2.Response;

public class ApiResponse<T> {

    public int status;
    public T data;
    public Throwable throwable;

    public String errorCode = "200";
    public String errorDescription = "Something went wrong";

    public ApiResponse(int status, T data, Throwable throwable) {
        this.status = status;
        this.data = data;
        this.throwable = throwable;
        if(throwable!=null){
        parseException(throwable);}
    }

    private void parseException(Throwable throwable) {
        if (throwable != null) {
            if (throwable instanceof SocketTimeoutException) {
                this.errorDescription = "Oooops! We couldn’t capture your request in time. Please try again.";
                this.errorCode = "No_internet";

            } else if (throwable instanceof MalformedJsonException) {
                this.errorDescription = "Oops! We hit an error. Try again later.";
                this.errorCode = "malformed_json";

            } else if (throwable instanceof IOException) {
                this.errorDescription = "Oh! You are not connected to a wifi or cellular data network. Please connect and try again";
                this.errorCode = "No_internet";
            } else if (throwable instanceof HttpException) {

                if (((HttpException) throwable).code() == 500) {
                    this.errorDescription = "Internal Server Error";
                    this.errorCode = String.valueOf(((Response) ((HttpException) throwable).response()).code());
                } else {
                    try {
                        this.errorDescription = ((Response) ((HttpException) throwable).response()).errorBody().string();
                        this.errorCode = String.valueOf(((Response) ((HttpException) throwable).response()).code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }

    }
}
